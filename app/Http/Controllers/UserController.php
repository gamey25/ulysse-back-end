<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Broadcasting\Broadcasters\AblyBroadcaster;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function getAllUser()
    {
        $listUser = User::all()->toJson();
        return response($listUser, 200);
    }

    public function getUserByEmailAndPassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message_validator' => $validator->errors(),
            ], 401);
        }

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $success['token'] = $user->createToken("appToken")->plainTextToken;
            //After successfull authentication, notice how I return json parameters
            return response()->json([
                'success' => true,
                'token' => $success,
                'user' => $user
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message_simple' => "Verifier l'email et le mot de passe",
            ], 401);
        }

    }


    public function registerUser(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'second_name' => 'required',
            'phone_number' => 'required|unique:users',
            //regex:/(0)[0-9]{10}/
            'poste' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),
            ], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken("appToken")->plainTextToken;
        ;
        return response()->json([
            'success' => true,
            'token' => $success,
            'user' => $user
        ], 200);
    }

    public function logout(Request $request)
    {

        return response()->json([
            "success" => true,
            "message" => "Echec deconnexion",
            "user" => $request->user()
        ]);
        if (Auth::user()) {
            $user = Auth::user();
            $user->tokens()->delete();
            return response()->json([
                "success" => true,
                "message" => "Deconnexion reussie"
            ]);
        } else {
            return response()->json([
                "success" => true,
                "message" => "Echec deconnexion"
            ]);
        }
    }

}
