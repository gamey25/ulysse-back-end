<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->group(function () {

    Route::get(
        '/user',
        function (Request $request) {
            return $request->user();
        }
    );
    Route::get('/user/logout', 'App\Http\Controllers\UserController@logout');
});

//Route::get('/user/logout', 'App\Http\Controllers\UserController@logout');
Route::post('/user/register-user', 'App\Http\Controllers\UserController@registerUser');
Route::post('/user/login', 'App\Http\Controllers\UserController@getUserByEmailAndPassword');

Route::get('/all-user', 'App\Http\Controllers\UserController@getAllUser');
