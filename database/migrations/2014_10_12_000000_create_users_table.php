<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->default('Henry');
            $table->string('second_name')->default('Matip');
            $table->string('phone_number')->default('+237 656 551 787');
            $table->string('poste')->unique()->default('Client');
            $table->string('email')->unique()->default('henrymatip90@gmail.com');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->default('pasword');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
